package services

import (
	"bitbucket.org/kaspro-sdk/app/types"
	opLogging "bitbucket.org/kaspro-sdk/logging"
	"bitbucket.org/kaspro-sdk/utils"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

var log = opLogging.MustGetLogger("kaspro-sdk")

type ThirdParty interface {
	HitClient(url url.URL) ([]byte, *types.GeneralResponse)
}

type ClientParty struct {
	UniqueID    string
	Localize    string
	ClientName  string
	HttpMethod  string
	UrlApi      string
	HttpClient  http.Client
	Headers     []map[string]string
	RequestBody io.Reader
}

func (c *ClientParty) SetApiUrl() (*url.URL, *types.GeneralResponse) {
	url, err := url.Parse(c.UrlApi)
	if err != nil {
		log.Errorf(c.UniqueID, "Error occurred %s ", err.Error())
		return nil, utils.GetResponseMessage(c.Localize, http.StatusInternalServerError, 99)
	}
	return url, nil
}

func (c *ClientParty) HitClient(url url.URL) ([]byte, *types.GeneralResponse) {

	request, err := http.NewRequest(c.HttpMethod, url.String(), c.RequestBody)
	if err != nil {
		log.Errorf(c.UniqueID, "Error occurred %s ", err.Error())
		return nil, utils.GetResponseMessage(c.Localize, http.StatusInternalServerError, 99)
	}

	for _, element := range c.Headers {
		for key, value := range element {
			if strings.ToLower(key) == "baseauth" {
				baseAuth := strings.Split(value, ":")
				request.SetBasicAuth(baseAuth[0], baseAuth[1])
			} else {
				request.Header.Set(key, value)
			}
		}
	}

	log.Infof(c.UniqueID, "===================================================== HIT "+strings.ToUpper(c.ClientName)+" API =======================================================")
	log.Infof(c.UniqueID, "HTTP METHOD %s ", request.Method)
	log.Infof(c.UniqueID, "URL %s ", request.URL)
	log.Infof(c.UniqueID, "HEADER %s ", request.Header)
	if request.Body != nil {
		log.Infof(c.UniqueID, "BODY %s ", request.Body)
	}
	log.Infof(c.UniqueID, "==========================================================================================================================")

	response, err := c.HttpClient.Do(request)
	if err != nil {
		log.Errorf(c.UniqueID, "Error occurred %s ", err.Error())
		return nil, utils.GetResponseMessage(c.Localize, http.StatusInternalServerError, 99)
	}

	if response.StatusCode != http.StatusOK {

		byteResult, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Errorf(c.UniqueID, "Error occurred %s ", err.Error())
			return nil, utils.GetResponseMessage(c.Localize, http.StatusInternalServerError, 99)
		}

		log.Infof(c.UniqueID, "================================================ RESPONSE OJK API ================================================")
		log.Infof(c.UniqueID, "GET RESPONSE ERROR FROM "+strings.ToUpper(c.ClientName)+" API %s", string(byteResult))
		log.Infof(c.UniqueID, "======================================================================================================================")

		errorMessage := types.GeneralResponse{
			HttpCode: response.StatusCode,
			Code:     response.StatusCode,
			Message:  response.Status,
		}
		return nil, &errorMessage
	}

	byteResult, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Errorf(c.UniqueID, "Error occurred %s ", err.Error())
		return nil, utils.GetResponseMessage(c.Localize, http.StatusInternalServerError, 99)
	}

	log.Infof(c.UniqueID, "================================================ RESPONSE OJK API ================================================")
	log.Infof(c.UniqueID, "GET RESPONSE FROM OJK API %s", string(byteResult))
	log.Infof(c.UniqueID, "======================================================================================================================")

	return byteResult, nil
}
