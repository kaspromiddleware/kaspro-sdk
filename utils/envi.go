package utils

import "bitbucket.org/kaspro-sdk/config"

func GetRunMode() string {
	serverMode := config.MustGetString("server.mode")
	return serverMode
}
