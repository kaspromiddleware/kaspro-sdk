package utils

import (
	opLogging "bitbucket.org/kaspro-sdk/logging"
)

var log = opLogging.MustGetLogger("kaspro-sdk")
