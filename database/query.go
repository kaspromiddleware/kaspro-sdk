package database

import (
	"database/sql"
	"github.com/jinzhu/gorm"
)

func Select(query string, args ...interface{}) (*gorm.DB, *sql.Rows, error) {
	DB, err := DBOpen()
	if err != nil {
		return DB, nil, err
	}
	res, err := DB.Raw(query, args...).Rows()
	return DB, res, err
}

func Insert(query string, args ...interface{}) (*gorm.DB, error) {
	DB, err := DBOpen()
	if err != nil {
		return DB, err
	}
	tx := DB.Begin()
	tx.Exec(query, args...)
	if err != nil {
		tx.Rollback()
		return DB, err
	}
	tx.Commit()
	return DB, nil
}

func Update(query string, args ...interface{}) (*gorm.DB, error) {
	DB, err := DBOpen()
	if err != nil {
		return DB, err
	}
	tx := DB.Begin()
	tx.Exec(query, args...)
	if err != nil {
		tx.Rollback()
		return DB, err
	}
	tx.Commit()
	return DB, nil
}

func Delete(query string, args ...interface{}) (*gorm.DB, error) {
	DB, err := DBOpen()
	if err != nil {
		return DB, err
	}
	DB.Exec(query, args...)
	if err != nil {
		return DB, err
	}
	return DB, nil
}
