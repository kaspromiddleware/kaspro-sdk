package database

import (
	"fmt"

	"bitbucket.org/kaspro-sdk/config"
	"bitbucket.org/kaspro-sdk/logging"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
	INTERNAL = "INTERNAL"
)

var log = logging.MustGetLogger("kaspro-sdk")

func InitDB() (*gorm.DB, *error) {
	db, err := DBOpen()
	if err != nil {
		log.Errorf(INTERNAL, "Error When Open DB %s ", err.Error())
		return nil, &err
	}
	return db, nil
}

func DBOpen() (db *gorm.DB, err error) {

	serverMode := config.MustGetString("server.mode")
	dbDriver := config.MustGetString(serverMode + ".db_driver")
	dbHost := config.MustGetString(serverMode + ".db_host")
	dbPort := config.MustGetString(serverMode + ".db_port")
	dbName := config.MustGetString(serverMode + ".db_name")
	dbUsername := config.MustGetString(serverMode + ".db_username")
	dbPassword := config.MustGetString(serverMode + ".db_password")

	if dbDriver == "postgres" {
		connStringPostgres := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=%s", dbUsername, dbPassword, dbName, dbHost, dbPort, "disable")
		db, err = gorm.Open("postgres", connStringPostgres)
	} else if dbDriver == "mysql" {
		connStringMysql := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", dbUsername, dbPassword, dbHost, dbPort, dbName)
		db, err = gorm.Open("mysql", connStringMysql)
	} else {
		log.Errorf(logging.INTERNAL, "No Database Selected!, Please check config.toml")
	}

	return db, err
}
